package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/akoksharov/ci-utils/cilib"
)

func main() {
	var key string
	var secret string
	var terraformDir string
	var arnPattern string

	flag.StringVar(&key, "aws-access-key-id", "", "AWS key id. Requires aws-secret-access-key to be set as well. (Defaults to AWS_ACCESS_KEY_ID)")
	flag.StringVar(&secret, "aws-secret-access-key", "", "AWS access secret. Requires aws-access-key-id to be set as well. (Defaults to AWS_SECRET_ACCESS_KEY)")
	flag.StringVar(&terraformDir, "terraform-dir", "", "Absolute or relative path to a directory containing terraform code. (Defaults to the current working dirrectory)")
	flag.StringVar(&arnPattern, "arn-pattern", "", "arn string. '*' to be substituted with account_id (Required)")
	flag.Parse()

	if arnPattern == "" {
		log.Fatalf("'arnPattern' is a required parameter")
	}

	if terraformDir == "" {
		terraformDir = "./"
	}

	if key == "" {
		key = os.Getenv("AWS_ACCESS_KEY_ID")
		if key == "" {
			log.Fatalf("'aws-access-key-id' is not set")
		}
	}

	if secret == "" {
		secret = os.Getenv("AWS_SECRET_ACCESS_KEY")
		if secret == "" {
			log.Fatalf("'aws-access-key-id' is not set")
		}
	}

	roleCred, err := cilib.AssumeRoleForTerraformCode(terraformDir, arnPattern)
	if err != nil {
		log.Fatalf("Error: '%s'", err)
	}

	if roleCred == nil {
		log.Fatal("Error: 'No account_id found'")
	}

	fmt.Printf("export AWS_ACCESS_KEY_ID=%s\n", roleCred.Key)
	fmt.Printf("export AWS_SECRET_ACCESS_KEY=%s\n", roleCred.Secret)
	fmt.Printf("export AWS_SESSION_TOKEN=%s\n", roleCred.Token)
	fmt.Printf("export AWS_SECURITY_TOKEN=%s\n", roleCred.Token)
}
