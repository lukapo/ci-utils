package cilib

import (
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

// RoleCredentials contains session credentials
type RoleCredentials struct {
	Key        string
	Secret     string
	Token      string
	Expiration time.Time
}

// AssumeRoleWithEnvCredentials returns temporary credentials for a role
// using environment variables
// * Access Key ID:     AWS_ACCESS_KEY_ID or AWS_ACCESS_KEY
// * Secret Access Key: AWS_SECRET_ACCESS_KEY or AWS_SECRET_KEY
func AssumeRoleWithEnvCredentials(region string, arn string) (*RoleCredentials, error) {
	creds := credentials.NewEnvCredentials()
	return assumeRole(creds, region, arn)
}

// AssumeRoleWithCredentials returns temporary credentials for a role
// using provided key_id and secret_key
func AssumeRoleWithCredentials(key string, secret string, region string, arn string) (*RoleCredentials, error) {
	creds := credentials.NewStaticCredentials(key, secret, "")
	return assumeRole(creds, region, arn)
}

func assumeRole(creds *credentials.Credentials, region string, arn string) (*RoleCredentials, error) {
	conf := aws.NewConfig().
		WithRegion(region).
		WithCredentials(creds)

	sess, err := session.NewSession(conf)
	if err != nil {
		return nil, err
	}

	client := sts.New(sess)

	sessionName := strings.ReplaceAll(strings.ReplaceAll(arn, ":", "-"), "/", "@")

	assumeRoleInput := &sts.AssumeRoleInput{
		RoleArn:         aws.String(arn),
		RoleSessionName: aws.String(sessionName),
		DurationSeconds: aws.Int64(int64(1800)), // 30 min
	}

	assumeRole, err := client.AssumeRole(assumeRoleInput)
	if err != nil {
		return nil, err
	}

	return &RoleCredentials{
		Key:        *assumeRole.Credentials.AccessKeyId,
		Secret:     *assumeRole.Credentials.SecretAccessKey,
		Token:      *assumeRole.Credentials.SessionToken,
		Expiration: *assumeRole.Credentials.Expiration,
	}, nil
}
