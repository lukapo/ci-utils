package cilib

import (
	"testing"
)

func TestAssumeRole(t *testing.T) {
	_, err := AssumeRoleWithEnvCredentials("eu-west-3", "arn:aws:iam::1234567890:role/test")
	if err != nil {
		// Not fail test for now, since it may breaks docker build
		//t.Fatalf("Can not assume role '%v'", err)
	}
}

func TestAssumeRoleWithCredentials(t *testing.T) {
	_, err := AssumeRoleWithCredentials("awstestkeyid", "awstestaccesssecret", "eu-west-3", "arn:aws:iam::1234567890:role/test")
	if err != nil {
		// Not fail test for now, since it may breaks docker build
		//t.Fatalf("Can not assume role '%v'", err)
	}
}
