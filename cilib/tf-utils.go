package cilib

import (
	"errors"
	"io/ioutil"
	"path"
	"strings"

	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclparse"
)

// AccountInfo contains account id and region read from provider configuration
type AccountInfo struct {
	Region string
	IDs    []string
}

// GetAccountID looks for aws provider configuration in .tf files
// returns first found data. If not found nil is returned.
func GetAccountID(tfDirPath string) (*AccountInfo, error) {
	files, err := ioutil.ReadDir(tfDirPath)
	if err != nil {
		return nil, err
	}

	result := &AccountInfo{
		Region: "",
		IDs:    make([]string, 1),
	}

	parser := hclparse.NewParser()

	for _, file := range files {
		if strings.ToLower(path.Ext(file.Name())) != ".tf" {
			continue
		}

		file, _ := parser.ParseHCLFile(path.Join(tfDirPath, file.Name()))
		if file == nil {
			continue
		}

		content, _, diags := file.Body.PartialContent(&hcl.BodySchema{
			Blocks: []hcl.BlockHeaderSchema{
				{
					Type:       "provider",
					LabelNames: []string{"name"},
				},
			},
		})

		for _, block := range content.Blocks {
			if block.Labels[0] != "aws" {
				continue
			}
			switch block.Type {
			case "provider":
				content, _, diags = block.Body.PartialContent(&hcl.BodySchema{
					Attributes: []hcl.AttributeSchema{
						{
							Name:     "region",
							Required: true,
						},
						{
							Name:     "allowed_account_ids",
							Required: true,
						},
					},
				})
				if diags != nil {
					return nil, diags
				}

				diags := gohcl.DecodeExpression(content.Attributes["region"].Expr, nil, &result.Region)
				if diags != nil {
					return nil, diags
				}
				diags = gohcl.DecodeExpression(content.Attributes["allowed_account_ids"].Expr, nil, &result.IDs)
				if diags != nil {
					return nil, diags
				}

				return result, nil
			}
		}
	}

	return nil, errors.New("provider definition is not found")
}
