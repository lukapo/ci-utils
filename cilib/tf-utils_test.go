package cilib

import (
	"io/ioutil"
	"os"
	"path"
	"testing"
)

const (
	tfProvider1 string = `
provider "aws" {
  region              = "eu-central-1"
  allowed_account_ids = ["111"]
}
data "aws_availability_zones" "available" {
  state = "available"
}
resource "aws_autoscaling_group" "this" {
  availability_zones   = data.aws_availability_zones.available.names
}

`
	tfProvider2 string = `
provider "aws" {
  region              = "eu-central-1"
}
`
	tfData string = `
data "aws_ami" "image" {
  owners      = ["amazon"]
  most_recent = true
}
`
)

func TestGetAccountID_Defined(t *testing.T) {

	testDir, err := ioutil.TempDir("/tmp", "ci-utils")
	if err != nil {
		t.Fatalf("Cant create temp directory '%v'", err)
	}
	defer os.RemoveAll(testDir)

	if ioutil.WriteFile(path.Join(testDir, "provider1.tf"), []byte(tfProvider1), 0644) != nil {
		t.Fatalf("Cant create provider1.tf '%v'", err)
	}
	if ioutil.WriteFile(path.Join(testDir, "data.tf"), []byte(tfData), 0644) != nil {
		t.Fatalf("Cant create data.tf '%v'", err)
	}

	accountInfo, err := GetAccountID(testDir)
	if err != nil {
		t.Fatalf("Failed to read account_id '%v'", err)
	}
	if accountInfo == nil {
		t.Fatal("Nothing returned")
	}
	if len(accountInfo.IDs) != 1 && accountInfo.IDs[0] != "111" && accountInfo.Region != "eu-central-1" {
		t.Fatalf("Unexpected result")
	}
}

func TestGetAccountID_Empty(t *testing.T) {

	testDir, err := ioutil.TempDir("/tmp", "ci-utils")
	if err != nil {
		t.Fatalf("Cant create temp directory '%v'", err)
	}
	defer os.RemoveAll(testDir)

	if ioutil.WriteFile(path.Join(testDir, "provider2.tf"), []byte(tfProvider2), 0644) != nil {
		t.Fatalf("Cant create provider2.tf '%v'", err)
	}
	if ioutil.WriteFile(path.Join(testDir, "data.tf"), []byte(tfData), 0644) != nil {
		t.Fatalf("Cant create data.tf '%v'", err)
	}

	accountInfo, err := GetAccountID(testDir)
	if err == nil {
		t.Fatal("Error is expected but not returned")
	}
	if accountInfo != nil {
		t.Fatalf("Unexpected result")
	}
}

func TestGetAccountID_NotTfFile(t *testing.T) {

	testDir, err := ioutil.TempDir("/tmp", "ci-utils")
	if err != nil {
		t.Fatalf("Cant create temp directory '%v'", err)
	}
	defer os.RemoveAll(testDir)

	if ioutil.WriteFile(path.Join(testDir, "provider1.txt"), []byte(tfProvider1), 0644) != nil {
		t.Fatalf("Cant create provider1.tf '%v'", err)
	}
	if ioutil.WriteFile(path.Join(testDir, "data.tf"), []byte(tfData), 0644) != nil {
		t.Fatalf("Cant create data.tf '%v'", err)
	}

	accountInfo, err := GetAccountID(testDir)
	if err == nil {
		t.Fatal("Error is expected but not returned")
	}
	if accountInfo != nil {
		t.Fatalf("Unexpected result")
	}
}
