package cilib

import (
	"strings"

	_ "github.com/gruntwork-io/terratest/modules/terraform" // just to get requirements cached
)

// AssumeRoleForTerraformCode does the following:
// - look through terraform code in tfDir
// - extracts allowed_account_ids and region from aws provider configuration
// - creates arn string by substituting first appearence of '*' in arnPattern with first account_id found
// returns session RoleCredentials
func AssumeRoleForTerraformCode(tfDir string, arnPattern string) (*RoleCredentials, error) {

	accounInfo, err := GetAccountID(tfDir)
	if err != nil {
		return nil, err
	}

	arn := strings.Replace(arnPattern, "*", accounInfo.IDs[0], 1)

	return AssumeRoleWithEnvCredentials(accounInfo.Region, arn)
}
